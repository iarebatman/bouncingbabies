extends KinematicBody2D

export (int) var INITIAL_MIN_SPEED
export (int) var INITIAL_MAX_SPEED


const GRAVITY = 200
#var speed = 4
var velocity = Vector2()

func _ready():
	$VisibilityNotifier2D.connect("screen_exited", self, "_on_screen_exit")
	pass

func _on_screen_exit():
	queue_free()
	
func start(dir, pos):
	rotation = dir
	position = pos
	velocity = Vector2(rand_range(INITIAL_MIN_SPEED, INITIAL_MAX_SPEED), 0).rotated(rotation)
	#velocity = Vector2(speed, 0).rotated(rotation)
	
func _physics_process(delta):
	
	velocity.y += delta * GRAVITY
	var motion = velocity * delta;
	
	var collision = move_and_collide(motion)
	if collision:
		if collision.collider.has_method("hit"):
			collision.collider.hit(self)
			velocity = velocity.bounce(collision.normal)
			
		
	

	
	
