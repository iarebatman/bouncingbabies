extends CanvasLayer

signal start_game

func show_message(text):
	$MessageLabel.text = text
	$MessageLabel.show()
	$MessageTimer.start()

func update_score(score):
	$ScoreTextLabel/ScoreLabel.text = str(score)
	
func _ready():
	$PlayButton.connect("pressed", self, "_on_play_button_pressed")
	$MessageTimer.connect("timeout", self, "_on_message_timer_timeout")
	pass

func _on_play_button_pressed():
	$PlayButton.hide()
	emit_signal("start_game")
	
func _on_message_timer_timeout():
	$MessageLabel.hide()


