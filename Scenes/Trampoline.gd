extends KinematicBody2D


export (int) var SPEED

var velocity = Vector2()
var screensize 
func _ready():
	screensize = get_viewport_rect().size
    
    # Called every time the node is added to the scene.
    # Initialization here
	# connect("body_entered", self, "_on_body_enter")
	pass

func _physics_process(delta):
	
	screensize = get_viewport_rect().size
	
	velocity = Vector2()
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * SPEED
 

	position += velocity * delta
	position.x = clamp(position.x, 375, screensize.x - 125)
   
	pass

func hit(body):
	$Audio.play()
	

#func _on_body_enter(body):
	#$Audio.play()
	
	
	#exaggerate the 'bounce'
	#body.apply_impulse(Vector2(1, 1), Vector2(-body.linear_velocity.x*0.5, -120))
	#body.apply_impulse(direction, Vector2(body.position.x, ))
    
    