extends Sprite

signal baby_rescued

func _ready():
	$Area.connect("body_entered", self, "rescue")
	
func rescue(baby):
	emit_signal("baby_rescued")
	baby.queue_free()
