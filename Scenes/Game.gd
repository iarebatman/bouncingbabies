extends CanvasLayer

export (PackedScene) var Baby

var score = 0

func _ready():
	randomize()
	$BabyTimer.connect("timeout", self, "_on_baby_timer_timeout")
	$HUD.connect("start_game", self, "_new_game")
	$Box.connect("baby_rescued", self, "_on_baby_rescued")
	pass

func update_score(score):
	score = score
	$HUD.update_score(score)
	

func _new_game():
	update_score(0)
	$HUD.show_message("Get Ready!")
	$BabyTimer.start()
	$Trampoline.show()
	
func _game_over():
	$BabyTimer.stop()
	$Trampoline.hide()

func _on_baby_rescued():
	score += 100
	update_score(score)

func _on_baby_timer_timeout():
	$BabyPath/SpawnLocations.set_offset(randi())
	
	var baby = Baby.instance()
	add_child(baby)
	var position = $BabyPath/SpawnLocations.position
	#baby.start(3, position, rand_range(baby.INITIAL_MIN_SPEED, baby.INITIAL_MAX_SPEED))
	baby.start(3, position)
	
	
	
	#baby.move_and_collide(Vector2(rand_range(baby.INITIAL_MIN_SPEED, baby.INITIAL_MAX_SPEED), 0).rotated(direction))
	#baby.set_linear_velocity(Vector2(rand_range(baby.INITIAL_MIN_SPEED, baby.INITIAL_MAX_SPEED), 0).rotated(direction))
    
